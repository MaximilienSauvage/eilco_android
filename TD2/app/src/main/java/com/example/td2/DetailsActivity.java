package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    NewsListApplication app;
    TextView displayLogin;
    String login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (NewsListApplication) getApplicationContext();
        setContentView(R.layout.activity_details);
        displayLogin = (TextView) findViewById(R.id.displayLogin);
        login = app.getLogin();
        displayLogin.setText(login);

        Button butDetOk = findViewById(R.id.butDetOk);
        butDetOk.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.butDetOk:
                intent = new Intent(this, NewsActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }
}
