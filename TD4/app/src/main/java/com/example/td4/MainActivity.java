package com.example.td4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import android.os.Bundle;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Callback;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        GitHubClient client = retrofit.create(GitHubClient.class);
        Call<List<RepoList>> call = client.UserRepositories("JakeWharton");
        Toast.makeText(MainActivity.this, "Loading ...", Toast.LENGTH_SHORT).show();

        call.enqueue(new Callback<List<RepoList>>() {
            @Override
            public void onResponse(Call<List<RepoList>> call, Response<List<RepoList>> response) {
                List<RepoList> list = response.body();
                if(list !=null) {

                    //On donne la liste a afficher au RecyclerView
                    RecyclerView myRecyclerView = findViewById(R.id.repoView);
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    myRecyclerView.setAdapter(new RepoListAdapter(list));

                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                }else{
                    Toast.makeText(MainActivity.this,"Error : No Repository found.",Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<List<RepoList>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error : Github Connection failed", Toast.LENGTH_SHORT).show();
                List<RepoList> list = null;
            }
        });
    }
}
