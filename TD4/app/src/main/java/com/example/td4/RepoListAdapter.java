package com.example.td4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListHolder> {

    private List<RepoList> repositories;

    public RepoListAdapter(List<RepoList> repoz){
        this.repositories = repoz;
    }

    @NonNull
    @Override
    public RepoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, parent, false);
        return new RepoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoListHolder holder, int position) {
        holder.display(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }
}
