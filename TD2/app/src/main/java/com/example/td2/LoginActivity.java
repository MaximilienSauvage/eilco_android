package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText fieldLogin;
    EditText fieldPassword;
    NewsListApplication app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (NewsListApplication) getApplicationContext();

        setContentView(R.layout.activity_login);

        Button button = findViewById(R.id.butLogin);
        button.setOnClickListener(this);

        fieldLogin = findViewById(R.id.field_idLogin);
        fieldPassword = findViewById(R.id.field_password);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.butLogin:
                intent = new Intent(this, NewsActivity.class);
                app.setLogin(fieldLogin.getText().toString());
                intent.putExtra("login",fieldLogin.getText().toString());
                intent.putExtra("pass",fieldPassword.getText().toString());
                startActivity(intent);
                this.finish();
                break;
        }
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
    }

    private static final String TAG = "LoginScreen ";
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        //Log.i(TAG, "terminaison de l'activité "+getLocalClassName());
    }
}