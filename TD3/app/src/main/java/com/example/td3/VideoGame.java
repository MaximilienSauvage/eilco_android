package com.example.td3;

public class VideoGame {

    String name;
    float price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    VideoGame(String n,float prix)
    {
        this.name = n;
        this.price = prix;
    }

}
