package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URL;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    String login;
    String pass;
    NewsListApplication app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (NewsListApplication) getApplicationContext();
        setContentView(R.layout.activity_news);

        Button butLogout = findViewById(R.id.butLogout);
        butLogout.setOnClickListener(this);

        Button butAbout = findViewById(R.id.butAbout);
        butAbout.setOnClickListener(this);

        Button butDetails = findViewById(R.id.butDetails);
        butDetails.setOnClickListener(this);

//        Bundle b = getIntent().getExtras();
//
//        if(b!=null) {
//            login = b.getString("login");
//            pass =  b.getString("pass");
//        }
        login = app.getLogin();
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.butLogout:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.butAbout:{
                String url = "http://android.busin.fr/";
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;
            }
            case R.id.butDetails:{
                intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

}
