package com.example.td4;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.Call;
import java.util.List;

public interface GitHubClient {

    @GET("users/{username}/repos")
    Call<List<RepoList>> UserRepositories(@Path("username") String username);
}
