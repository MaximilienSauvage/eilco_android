package com.example.td3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<VideoGame> mesJeux = new ArrayList<VideoGame>();
        mesJeux.add(new VideoGame("FF7",40));
        mesJeux.add(new VideoGame("FF9",25));
        mesJeux.add(new VideoGame("RaymanCollectorEdition",100));
        mesJeux.add(new VideoGame("Benoit's Story",70));

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
    }
}
