package com.example.td4;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoListHolder extends RecyclerView.ViewHolder {
    private TextView repoName;

    public RepoListHolder(@NonNull View iView) {
        super(iView);
        repoName = iView.findViewById(R.id.name);
    }
    void display(RepoList repoStr){
        repoName.setText(repoStr.getRepoName());
    }
}
