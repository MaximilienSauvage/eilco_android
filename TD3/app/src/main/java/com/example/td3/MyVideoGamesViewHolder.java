package com.example.td3;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder {
    private TextView mNameTV;
    private TextView mPriceTV;
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);
        mNameTV = itemView.findViewById(R.id.name);
        mPriceTV = itemView.findViewById(R.id.price);
    }
    void display(VideoGame jeuVideo){
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
    }
}
